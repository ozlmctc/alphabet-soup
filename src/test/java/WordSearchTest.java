import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import java.io.ByteArrayInputStream;

import java.io.InputStream;

class WordSearchTest {

    @Test
    void runAlphabetSoup() {
        String input = "5x5\n" +
                "H A S D F\n" +
                "G E Y B H\n" +
                "J K L Z X\n" +
                "C V B L N\n" +
                "G O O D O\n" +
                "HELLO\n" +
                "GOOD\n" +
                "BYE\n"+
                "GVLB\n"+
                "OLL\n"+
                "FBL\n";

        String expectedOutput = "HELLO 0:0 4:4\n" +
                "GOOD 4:0 4:3\n" +
                "BYE 1:3 1:1\n"+
                "GVLB 4:0 1:3\n"+
                "OLL 4:4 2:2\n"+
                "FBL 0:4 2:2\n";

        InputStream inputStream = new ByteArrayInputStream(input.getBytes());
        System.setIn(inputStream);

        String actualOutput = WordSearch.runAlphabetSoup();

        Assertions.assertEquals(expectedOutput, actualOutput);
    }

}