import java.util.*;
public class WordSearch {
    public static String runAlphabetSoup() {
            Scanner scanner = new Scanner(System.in);
            // Read the dimensions of the character grid
            String dimensions = scanner.nextLine();
            int rows = Integer.parseInt(dimensions.split("x")[0]);
            int columns = Integer.parseInt(dimensions.split("x")[1]);

            // Read the character grid
            char[][] grid = new char[rows][columns];
            for (int i = 0; i < rows; i++) {
                String line = scanner.nextLine();
                String[] chars = line.split(" ");
                for (int j = 0; j < columns; j++) {
                    grid[i][j] = chars[j].charAt(0);
                }
            }

            // Read the list of words to find
            List<String> words = new ArrayList<>();
            while (scanner.hasNextLine()) {
                words.add(scanner.nextLine().replace(" ", ""));
            }

            // Find the words in the character grid
            StringBuilder output = new StringBuilder();
            for (String word : words) {
                boolean found = false;
                Map<String, Integer> rowsColumnsMap = new HashMap<>();
                rowsColumnsMap.put("startRow", -1);
                rowsColumnsMap.put("startCol", -1);
                rowsColumnsMap.put("endRow", -1);
                rowsColumnsMap.put("endCol", -1);

                // Check horizontal forwards
                if (searchHorizontalForward(grid, word, found, rowsColumnsMap)) {
                    found = true;
                }

                // Check horizontal backwards
                if (!found && searchHorizontalBackward(grid, word, found, rowsColumnsMap)) {
                    found = true;
                }

                // Check vertical forwards
                if (!found && searchVerticalForward(grid, word, found, rowsColumnsMap)) {
                    found = true;
                }

                // Check vertical backwards
                if (!found && searchVerticalBackward(grid, word, found, rowsColumnsMap)) {
                    found = true;
                }

                // Check diagonal forwards
                if (!found && searchTopLeftToBottomRightDiagonalForward(grid, word, found, rowsColumnsMap)) {
                    found = true;
                }

                if (!found && searchTopRightToBottomLeftDiagonalForward(grid, word, found, rowsColumnsMap)) {
                    found = true;
                }

                // Check diagonal backwards
                if (!found && searchBottomRightToTopLeftDiagonalBackward(grid, word, found, rowsColumnsMap)) {
                    found = true;
                }

                if (!found && searchBottomLeftToTopRightDiagonalBackward(grid, word, found, rowsColumnsMap)) {
                    found = true;
                }

                // Output the results for the current word
                output.append(word).append(" ");
                if (found) {
                    output.append(rowsColumnsMap.get("startRow")).append(":").append(rowsColumnsMap.get("startCol")).append(" ")
                            .append(rowsColumnsMap.get("endRow")).append(":").append(rowsColumnsMap.get("endCol"));
                } else {
                    output.append("-1:-1 -1:-1");
                }
                output.append("\n");
            }

            scanner.close();
            return output.toString();
        }



    private static boolean searchHorizontalForward(char[][] grid, String word, boolean found, Map<String, Integer> rowsColumns) {
        int columns = grid.length;
        int rows = grid[0].length;
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j <= columns - word.length(); j++) {
                boolean match = true;
                for (int k = 0; k < word.length(); k++) {
                    if (word.charAt(k) != grid[i][j + k]) {
                        match = false;
                        break;
                    }
                }
                if (match) {
                    found = true;
                    rowsColumns.replace("startRow", i);
                    rowsColumns.replace("startCol", j);
                    rowsColumns.replace("endRow", i);
                    rowsColumns.replace("endCol", j + word.length() - 1);
                    break;
                }
            }
            if (found) {
                break;
            }
        }
        return found;
    }

    private static boolean searchHorizontalBackward(char[][] grid, String word, boolean found, Map<String, Integer> rowsColumns) {
        int columns = grid.length;
        int rows = grid[0].length;
        for (int i = 0; i < rows; i++) {
            for (int j = columns - 1; j >= word.length() - 1; j--) {
                boolean match = true;
                for (int k = 0; k < word.length(); k++) {
                    if (word.charAt(k) != grid[i][j - k]) {
                        match = false;
                        break;
                    }
                }
                if (match) {
                    found = true;
                    rowsColumns.replace("startRow", i);
                    rowsColumns.replace("startCol", j);
                    rowsColumns.replace("endRow", i);
                    rowsColumns.replace("endCol", j - word.length() + 1);
                    break;
                }
            }
            if (found) {
                break;
            }
        }
        return found;
    }

    private static boolean searchVerticalForward(char[][] grid, String word, boolean found, Map<String, Integer> rowsColumns) {
        int columns = grid.length;
        int rows = grid[0].length;
        for (int i = 0; i <= rows - word.length(); i++) {
            for (int j = 0; j < columns; j++) {
                boolean match = true;
                for (int k = 0; k < word.length(); k++) {
                    if (word.charAt(k) != grid[i + k][j]) {
                        match = false;
                        break;
                    }
                }
                if (match) {
                    found = true;
                    rowsColumns.replace("startRow", i);
                    rowsColumns.replace("startCol", j);
                    rowsColumns.replace("endRow", i + word.length() - 1);
                    rowsColumns.replace("endCol", j);
                    break;
                }
            }
            if (found) {
                break;
            }
        }
        return found;
    }

    private static boolean searchVerticalBackward(char[][] grid, String word, boolean found, Map<String, Integer> rowsColumns) {
        int columns = grid.length;
        int rows = grid[0].length;
        for (int i = rows - 1; i >= word.length() - 1; i--) {
            for (int j = 0; j < columns; j++) {
                boolean match = true;
                for (int k = 0; k < word.length(); k++) {
                    if (word.charAt(k) != grid[i - k][j]) {
                        match = false;
                        break;
                    }
                }
                if (match) {
                    found = true;
                    rowsColumns.replace("startRow", i);
                    rowsColumns.replace("startCol", j);
                    rowsColumns.replace("endRow", i - word.length() + 1);
                    rowsColumns.replace("endCol", j);
                    break;
                }
            }
            if (found) {
                break;
            }
        }
        return found;
    }


    private static boolean searchTopLeftToBottomRightDiagonalForward(char[][] grid, String word, boolean found, Map<String, Integer> rowsColumns) {
        int columns = grid.length;
        int rows = grid[0].length;
        for (int i = 0; i <= rows - word.length(); i++) {
            for (int j = 0; j <= columns - word.length(); j++) {
                boolean match = true;
                for (int k = 0; k < word.length(); k++) {
                    if (word.charAt(k) != grid[i+k][j+k]) {
                        match = false;
                        break;
                    }
                }
                if (match) {
                    found = true;
                    rowsColumns.replace("startRow", i);
                    rowsColumns.replace("startCol", j);
                    rowsColumns.replace("endRow", i + word.length() - 1);
                    rowsColumns.replace("endCol", j + word.length() - 1);
                    break;
                }
            }
            if (found) {
                break;
            }
        }
        return found;
    }
    private static boolean searchTopRightToBottomLeftDiagonalForward(char[][] grid, String word, boolean found, Map<String, Integer> rowsColumns) {
        int columns = grid.length;
        int rows = grid[0].length;
        for (int i = 0; i <= rows - word.length(); i++) {
            for (int j = word.length() - 1; j < columns; j++) {
                boolean match = true;
                for (int k = 0; k < word.length(); k++) {
                    if (word.charAt(k) != grid[i+k][j-k]) {
                        match = false;
                        break;
                    }
                }
                if (match) {
                    found = true;
                    rowsColumns.replace("startRow", i);
                    rowsColumns.replace("startCol", j);
                    rowsColumns.replace("endRow", i + word.length() - 1);
                    rowsColumns.replace("endCol", j - word.length() + 1);
                    break;
                }
            }
            if (found) {
                break;
            }
        }
        return found;
    }

    private static boolean searchBottomRightToTopLeftDiagonalBackward(char[][] grid, String word, boolean found, Map<String, Integer> rowsColumns) {
        int columns = grid.length;
        int rows = grid[0].length;
        for (int i = word.length() - 1; i < rows; i++) {
            for (int j = word.length() - 1; j < columns; j++) {
                boolean match = true;
                for (int k = 0; k < word.length(); k++) {
                    if (word.charAt(k) != grid[i - k][j - k]) {
                        match = false;
                        break;
                    }
                }
                if (match) {
                    found = true;
                    rowsColumns.replace("startRow", i);
                    rowsColumns.replace("startCol", j);
                    rowsColumns.replace("endRow", i - word.length() + 1);
                    rowsColumns.replace("endCol", j - word.length() + 1);
                    break;
                }
            }
            if (found) {
                break;
            }
        }
        return found;}

    private static boolean searchBottomLeftToTopRightDiagonalBackward(char[][] grid, String word, boolean found, Map<String, Integer> rowsColumns) {
        int columns = grid.length;
        int rows = grid[0].length;
        for (int i = word.length() - 1; i < rows; i++) {
            for (int j = 0; j <= columns - word.length(); j++) {
                boolean match = true;
                for (int k = 0; k < word.length(); k++) {
                    if (word.charAt(k) != grid[i - k][j + k]) {
                        match = false;
                        break;
                    }
                }
                if (match) {
                    found = true;
                    rowsColumns.replace("startRow", i);
                    rowsColumns.replace("startCol", j);
                    rowsColumns.replace("endRow", i - word.length() + 1);
                    rowsColumns.replace("endCol", j + word.length() - 1);
                    break;
                }
            }
            if (found) {
                break;
            }
        }
        return found;
    }


}
